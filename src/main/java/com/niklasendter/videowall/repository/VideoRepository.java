package com.niklasendter.videowall.repository;

import com.niklasendter.videowall.model.User;
import com.niklasendter.videowall.model.Video;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VideoRepository extends JpaRepository<Video, String> {
    List<Video> findAllByUser(User user);
}
