package com.niklasendter.videowall.repository;

import com.niklasendter.videowall.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
