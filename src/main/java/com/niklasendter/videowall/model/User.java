package com.niklasendter.videowall.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class User {

    @Id
    @Column(nullable = false, unique = true, updatable = false, columnDefinition = "VARCHAR(30)")
    private String id;

    @NotBlank(message="Password is not valid!")
    @Column(nullable = false)
    private String password;

    private Instant createdAt;

    @OneToMany(mappedBy = "user")
    private List<Video> videos;

    @OneToMany(mappedBy = "user")
    private List<VideoComment> comments;
    @OneToMany(mappedBy = "user")
    private List<VideoLike> likes;

}
