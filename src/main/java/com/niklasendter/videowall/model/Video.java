package com.niklasendter.videowall.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Video {

    @Id
    @Column(unique = true, nullable = false, updatable = false, columnDefinition = "VARCHAR(12)")
    private String id;

    private String title;
    @Lob
    private String description;

    private String username;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "video")
    private List<VideoComment> comments;

    private Long likes;
    @OneToMany(mappedBy = "user")
    private List<VideoLike> videoLike;

    private String artwork;

    private Boolean isYoutube;
    private String youtubeId;

    private Boolean processed;

}
