package com.niklasendter.videowall.security;

import com.niklasendter.videowall.exception.AuthException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;

@Service
public class JwtProvider {

    private KeyStore keyStore;

    @PostConstruct
    public void init() throws AuthException {
        try{
            keyStore = KeyStore.getInstance("JKS");
            InputStream inputStream = getClass().getResourceAsStream("/security/videowall.jks");
            keyStore.load(inputStream, "3exAIV3jK99d9J85SXqt".toCharArray());
        }catch(KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e){
            throw new AuthException("Exception occurred while loading keystore: " + e);
        }
    }

    public String generateToken(Authentication authentication){
        User principal = (User) authentication.getPrincipal();
        return Jwts.builder().setSubject(principal.getUsername()).signWith(getPrivateKey()).compact();
    }

    private PrivateKey getPrivateKey(){
        try{
            return (PrivateKey) keyStore.getKey("videowall", "3exAIV3jK99d9J85SXqt".toCharArray());
        }catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e){
            throw new AuthException("Exception occurred while retrieving public key from keystore");
        }
    }


    public boolean validateToken(String jwt){
        Jwts.parser().setSigningKey(getPrivateKey()).parseClaimsJws(jwt);
        return true;
    }

    public String getUsernameFromJWT(String jwt){
        Claims claims = Jwts.parser().setSigningKey(getPrivateKey()).parseClaimsJws(jwt).getBody();
        return claims.getSubject();
    }

}
