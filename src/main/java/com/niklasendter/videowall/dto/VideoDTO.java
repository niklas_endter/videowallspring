package com.niklasendter.videowall.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class VideoDTO {

    private String id;
    private String title;
    private String description;
    private String username;

    private Long likes;

    private String artwork;

    private Boolean isYoutube;
    private String youtubeId;

}
