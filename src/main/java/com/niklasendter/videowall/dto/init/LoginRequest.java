package com.niklasendter.videowall.dto.init;

import lombok.Getter;

@Getter
public class LoginRequest {

    private String username;
    private String password;

}
