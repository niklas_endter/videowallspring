package com.niklasendter.videowall.dto.init;

import lombok.Getter;

@Getter
public class RegisterRequest {

    private String username;
    private String password;

}
