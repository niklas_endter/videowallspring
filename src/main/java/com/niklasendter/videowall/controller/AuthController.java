package com.niklasendter.videowall.controller;

import com.google.gson.Gson;
import com.niklasendter.videowall.dto.init.AuthenticationResponse;
import com.niklasendter.videowall.dto.init.LoginRequest;
import com.niklasendter.videowall.dto.init.RegisterRequest;
import com.niklasendter.videowall.service.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/auth/")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "signup", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<AuthenticationResponse> signUp(@RequestParam(name = "json") String registerRequest,
                                                         @RequestParam(name = "file") MultipartFile file){
        AuthenticationResponse authenticationResponse = authService.signUp(new Gson().fromJson(registerRequest, RegisterRequest.class), file);
        return new ResponseEntity<>(authenticationResponse, HttpStatus.OK);
    }

    @PostMapping("login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody LoginRequest loginRequest){
        System.out.println("loginRequest = " + loginRequest);
        AuthenticationResponse authenticationResponse = authService.login(loginRequest);
        return new ResponseEntity<>(authenticationResponse, HttpStatus.OK);
    }

}
