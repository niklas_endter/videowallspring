package com.niklasendter.videowall.controller;

import com.niklasendter.videowall.dto.VideoDTO;
import com.niklasendter.videowall.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api/v/")
public class VideoController {

    private final VideoService videoService;

    @Autowired
    public VideoController(VideoService videoService) {
        this.videoService = videoService;
    }

    @GetMapping("start")
    public ResponseEntity<List<VideoDTO>> getStartVideos(@RequestParam(required = false) Integer offset){
        List<VideoDTO> videos = videoService.getStartVideos(offset);
        return new ResponseEntity<>(videos, HttpStatus.OK);
    }

    @PostMapping("add/youtube")
    public ResponseEntity<VideoDTO> addYoutubeVideo(@RequestBody VideoDTO videoDTO){
        VideoDTO video = videoService.addYoutubeVideo(videoDTO);
        return new ResponseEntity<>(video, HttpStatus.OK);
    }

    @PostMapping(value = "add/custom", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<VideoDTO> addCustomVideo(@RequestParam(name = "json") String json,
                                                   @RequestParam(name = "video") MultipartFile video,
                                                   @RequestParam(name = "thumbnail") MultipartFile thumbnail){
        VideoDTO processed = videoService.addCustomVideo(json, video, thumbnail);
        return new ResponseEntity<>(processed, HttpStatus.OK);
    }

    @GetMapping("find")
    public ResponseEntity<VideoDTO> findVideo(@RequestParam String id){
        VideoDTO video = this.videoService.findVideo(id);
        return new ResponseEntity<>(video, HttpStatus.OK);
    }

}
