package com.niklasendter.videowall.service;

import com.niklasendter.videowall.dto.VideoDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface VideoService {
    List<VideoDTO> getStartVideos(Integer offset);

    VideoDTO addYoutubeVideo(VideoDTO videoDTO);
    VideoDTO addCustomVideo(String json, MultipartFile video, MultipartFile thumbnail);

    VideoDTO findVideo(String id);

}
