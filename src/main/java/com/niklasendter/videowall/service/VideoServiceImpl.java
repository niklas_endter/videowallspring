package com.niklasendter.videowall.service;

import com.google.gson.Gson;
import com.niklasendter.videowall.dto.VideoDTO;
import com.niklasendter.videowall.exception.VideoNotFoundException;
import com.niklasendter.videowall.model.User;
import com.niklasendter.videowall.model.Video;
import com.niklasendter.videowall.model.VideoComment;
import com.niklasendter.videowall.model.VideoLike;
import com.niklasendter.videowall.repository.VideoRepository;
import com.niklasendter.videowall.service.auth.AuthService;
import com.niklasendter.videowall.service.auth.AuthServiceImpl;
import com.niklasendter.videowall.service.auth.MediaService;
import com.niklasendter.videowall.util.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VideoServiceImpl implements VideoService{

    private final VideoRepository videoRepository;
    private final AuthService authService;
    private final MediaService mediaService;

    @Autowired
    public VideoServiceImpl(VideoRepository videoRepository, AuthService authService, MediaService mediaService) {
        this.videoRepository = videoRepository;
        this.authService = authService;
        this.mediaService = mediaService;
    }

    @Override
    public List<VideoDTO> getStartVideos(Integer offset) {

        List<Video> videos = videoRepository.findAllByUser(authService.getSelfUser());


        // IS EMPTY
        if (videos.isEmpty())
            return new ArrayList<>();


        // HANDLE OFFSET
        if (offset == null)
            offset = 0;

        if (offset+20 > videos.size()-1){
            videos = videos.subList(offset, videos.size()-1);
        }else{
            videos = videos.subList(offset, offset+20);
        }


        // MAP VIDEOS
        return videos.stream().map(VideoServiceImpl::mapVideoToDTO).collect(Collectors.toList());
    }

    @Override
    public VideoDTO addYoutubeVideo(VideoDTO videoDTO) {

        Video video = new Video(
                Helpers.generateId(12),
                videoDTO.getTitle(),
                videoDTO.getDescription(),
                videoDTO.getUsername(),
                authService.getSelfUser(),
                new ArrayList<>(),
                0L,
                new ArrayList<>(),
                videoDTO.getArtwork(),
                true,
                videoDTO.getYoutubeId(),
                true
        );

        videoRepository.save(video);
        return mapVideoToDTO(video);
    }

    @Override
    public VideoDTO addCustomVideo(String json, MultipartFile video, MultipartFile thumbnail) {

        String videoId = Helpers.generateId(12);
        VideoDTO videoDTO = new Gson().fromJson(json, VideoDTO.class);

        Video newVideo = new Video(
                videoId,
                videoDTO.getTitle(),
                videoDTO.getDescription(),
                authService.getSelfUser().getId(),
                authService.getSelfUser(),
                new ArrayList<>(),
                0L,
                new ArrayList<>(),
                "http://localhost:8080/m/imgs/{s}/" + videoId + ".png",
                false,
                "",
                false
        );

        //PROCESS IMAGE
        mediaService.upload(videoId, thumbnail);

        //PROCESS VIDEO
        mediaService.upload(videoId, video);

        videoRepository.save(newVideo);
        return mapVideoToDTO(newVideo);
    }

    @Override
    public VideoDTO findVideo(String id) {
        Video video = videoRepository.findById(id).orElseThrow(() -> new VideoNotFoundException("No video with id: " + id));
        return mapVideoToDTO(video);
    }



    public static VideoDTO mapVideoToDTO(Video video){
        return new VideoDTO(
                video.getId(),
                video.getTitle(),
                video.getDescription(),
                video.getUsername(),
                video.getLikes(),
                video.getArtwork(),
                video.getIsYoutube(),
                video.getYoutubeId()
        );
    }

}
