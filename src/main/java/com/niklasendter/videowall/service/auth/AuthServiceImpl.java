package com.niklasendter.videowall.service.auth;

import com.niklasendter.videowall.dto.init.AuthenticationResponse;
import com.niklasendter.videowall.dto.init.LoginRequest;
import com.niklasendter.videowall.dto.init.RegisterRequest;
import com.niklasendter.videowall.exception.UserNotFoundException;
import com.niklasendter.videowall.model.User;
import com.niklasendter.videowall.repository.UserRepository;
import com.niklasendter.videowall.security.JwtProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Optional;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService{

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final PasswordEncoder passwordEncoder;
    private final MediaService mediaService;

    @Autowired
    public AuthServiceImpl(UserRepository userRepository, AuthenticationManager authenticationManager, JwtProvider jwtProvider, PasswordEncoder passwordEncoder, MediaService mediaService) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.passwordEncoder = passwordEncoder;
        this.mediaService = mediaService;
    }


    @Override
    @Transactional
    public AuthenticationResponse signUp(RegisterRequest registerRequest, MultipartFile file) {

        User user = new User(
                registerRequest.getUsername(),
                encodePassword(registerRequest.getPassword()),
                Instant.now(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        userRepository.save(user);

        if (!file.isEmpty()){
            mediaService.upload(registerRequest.getUsername(), file);
        }

        return getAuthenticationResponse(registerRequest.getUsername(), registerRequest.getPassword());
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    @Override
    public AuthenticationResponse login(LoginRequest loginRequest) {
        return getAuthenticationResponse(loginRequest.getUsername(), loginRequest.getPassword());
    }


    private AuthenticationResponse getAuthenticationResponse(String username, String password){
        UsernamePasswordAuthenticationToken upaToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(upaToken);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String authenticationToken = jwtProvider.generateToken(authentication);
        return new AuthenticationResponse(authenticationToken, username);
    }

    public Optional<org.springframework.security.core.userdetails.User> getCurrentUser() {
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Optional.of(principal);
    }

    public User getSelfUser(){
        org.springframework.security.core.userdetails.User spring_user = getCurrentUser().orElseThrow(() ->
                new RuntimeException("Authentication failed")
        );
        return userRepository.findById(spring_user.getUsername()).orElseThrow(() ->
                new UserNotFoundException("no user with id: " + spring_user.getUsername())
        );
    }

}
