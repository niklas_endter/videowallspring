package com.niklasendter.videowall.service.auth;

import com.niklasendter.videowall.exception.VideoNotFoundException;
import com.niklasendter.videowall.model.Video;
import com.niklasendter.videowall.repository.VideoRepository;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service
public class MediaServiceImpl implements MediaService{

    private File mRoot;

    private final VideoRepository videoRepository;

    public MediaServiceImpl(VideoRepository videoRepository) {
        this.videoRepository = videoRepository;
        createDirectories();
    }

    @Override
    public void upload(String name, MultipartFile file) {
        if ((Objects.requireNonNull(file.getContentType()).equals("image/png") || file.getContentType().equals("image/jpeg"))) {
            manageUploadImage(name, file);
        } else if (file.getContentType().equals("video/mp4")) {
            manageUploadVideo(name, file);
        }
    }

    private void manageUploadImage(String name, MultipartFile file){
        File dest = new File(this.mRoot + "/imgs/" + name + ".png");
        System.out.println("dest = " + dest);
        try (OutputStream os = new FileOutputStream(dest)) {
            os.write(file.getBytes());
            processImage(dest);
            os.close();
            boolean deleted = dest.delete();
            if (!deleted)
                System.out.println("Could not delete original file!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void manageUploadVideo(String name, MultipartFile file){
        File theDir = new File(this.mRoot + "/vids/" + name);
        if (theDir.mkdir()) {
            File dest = new File(this.mRoot + "/vids/" + name + "/" + "video.mp4");
            try (OutputStream os = new FileOutputStream(dest)) {
                os.write(file.getBytes());
                processVideo(name);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    private void processImage(File sourceFile) {
        Integer[] imageSizes = {75, 150, 450, 900};
        try {
            BufferedImage bufferedImage = ImageIO.read(sourceFile);
            for (Integer size: imageSizes) {
                BufferedImage outputImage = Scalr.resize(bufferedImage, size);
                String newFileName = FilenameUtils.getBaseName( sourceFile.getName())
                        + "."
                        + FilenameUtils.getExtension(sourceFile.getName());
                Path path = Paths.get(this.mRoot + "/imgs/" + size, newFileName);
                File newImageFile = path.toFile();
                ImageIO.write(outputImage, "png", newImageFile);
                outputImage.flush();
            }
        } catch (IOException e) {
            System.out.println("e = " + e);
        }
    }

    private void processVideo(String name){

        String[] command = new String[]{

                "ffmpeg",
                "-i", this.mRoot + "/vids/" + name + "/video.mp4",
                "-preset", "veryfast",
                "-keyint_min", "100",
                "-g", "100",
                "-sc_threshold", "0",
                "-r", "25",
                "-c:v", "libx264",
                "-pix_fmt", "yuv420p",
                "-crf", "21",

                "-map", "v:0", "-s:0", "960x540", "-maxrate:0", "2M", "-bufsize:0", "4M",
                "-map", "v:0", "-s:1", "1280x720", "-maxrate:1", "6M", "-bufsize:1", "12M",
                "-map", "v:0", "-s:2", "1920x1080", "-maxrate:2", "7.8M", "-bufsize:2", "15.6M",

                "-map", "a:0", "-map", "a:0", "-map", "a:0", "-c:a", "aac", "-b:a", "128k", "-ac", "2", "-ar", "44100",

                "-f", "hls", "-hls_time", "4", "-hls_playlist_type", "vod", "-hls_flags", "independent_segments",
                "-master_pl_name", "main.m3u8",
                "-var_stream_map", "v:0,a:0 v:1,a:1 v:2,a:2", this.mRoot + "/vids/" + name + "/stream_%v.m3u8"

        };

        Thread processingThread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {

                    Runtime rt = Runtime.getRuntime();
                    Process process = rt.exec(command);

                    BufferedReader stdError = new BufferedReader(new
                            InputStreamReader(process.getErrorStream()));

                    String s = null;
                    System.out.println("Errors (if any):\n");
                    while ((s = stdError.readLine()) != null) {
                        System.out.println(s);
                    }

                    process.waitFor();
                    System.out.println("Video Conversion finished...");
                    deleteVideo(name);


                    // SET PROCESSED
                    Video video = videoRepository.findById(name).orElseThrow(() -> new VideoNotFoundException("No video with id: " + name));
                    video.setProcessed(true);
                    videoRepository.save(video);

                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
        processingThread.start();

    }


    private void deleteVideo(String name){
        File file = new File(this.mRoot + "/vids/" + name + "/video.mp4");
        if (!file.delete()){
            System.out.println("Could not delete file!");
        }
    }


    private void createDirectories(){

        File mRoot = new File(System.getProperty("user.dir") + "/m/");

        this.mRoot = mRoot;

        if (!mRoot.exists()) {

            if (mRoot.mkdirs()) {

                File videoDir = new File(mRoot + "/vids");
                File imageDir = new File(mRoot + "/imgs");

                File dir75 = new File(mRoot + "/imgs/75");
                File dir150 = new File(mRoot + "/imgs/150");
                File dir450 = new File(mRoot + "/imgs/450");
                File dir900 = new File(mRoot + "/imgs/900");

                boolean createdVideo = videoDir.mkdirs();
                boolean createdImage = imageDir.mkdirs();

                dir75.mkdirs();
                dir150.mkdirs();
                dir450.mkdirs();
                dir900.mkdirs();

            }

        }
    }

}
