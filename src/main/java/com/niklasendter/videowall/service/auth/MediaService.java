package com.niklasendter.videowall.service.auth;

import org.springframework.web.multipart.MultipartFile;

public interface MediaService {
    void upload(String name, MultipartFile file);
}
