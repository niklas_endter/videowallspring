package com.niklasendter.videowall.service.auth;

import com.niklasendter.videowall.dto.init.AuthenticationResponse;
import com.niklasendter.videowall.dto.init.LoginRequest;
import com.niklasendter.videowall.dto.init.RegisterRequest;
import com.niklasendter.videowall.model.User;
import org.springframework.web.multipart.MultipartFile;

public interface AuthService {
    AuthenticationResponse signUp(RegisterRequest registerRequest, MultipartFile file);
    AuthenticationResponse login(LoginRequest loginRequest);

    User getSelfUser();
}
