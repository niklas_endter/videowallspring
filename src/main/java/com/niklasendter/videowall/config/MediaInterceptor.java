package com.niklasendter.videowall.config;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MediaInterceptor implements HandlerInterceptor {
    @Override
    public void postHandle(  HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler,
                             ModelAndView modelAndView){

        if (request.getRequestURI().startsWith("/m")){
            System.out.println("request = " + request.getRequestURI());
        }
    }
}
