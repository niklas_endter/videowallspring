package com.niklasendter.videowall.util;

public class Helpers {

    public static String generateId(int length){
        char[] letters = ("abcdefghijklmnopqrstuvwxyz"+"abcdefghijklmnopqrstuvwxyz".toUpperCase() + "0123456789").toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++){ sb.append(letters[randomInt(0, letters.length-1)]); }
        return sb.toString();
    }

    public static int randomInt(int min, int max){
        return min + (int)(Math.random() * ((max - min) + 1));
    }

}
